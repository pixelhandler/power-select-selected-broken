# Power-select-selected-broken

Reproduction of an issue using ember-power-select new than version v0.10.11

Original issue in my app…

- The selected item wasn’t shown as the selected option even though `onchange`
did update the selected item

Works in v0.10.11 but not in newer version.

Trying to isolate the issue results in worse where the select box is just gone.

- Example [Working Twiddle]
- Has a test as well, in the twiddle.json set `"enable-testing": true`

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Running Tests

* `ember test`
* `ember test --server`

## Bug Description

### Expected Behavior

The select box loads options that are an `Ember.ArrayProxy` instance with
content removed from a `RSVP.Promise`. The options for the power select component
are objects.

- Select box works normally
- Click and choose an option
- Click then type to search and enter to select
- Click then focus in search and search to narrow down results
- Acceptance test for the placeholder text should pass

### Actual Behavior

After upgrading past v0.10.11, to current beta release v1.0.0-beta.8 …

- Acceptance test for the placeholder text fails
- Current browsers do not even render the power select component
  - The element is gone missing

- Using Ember LTS version v2.4.6 and Ember CLI 2.7.0, and Node 4.4.5
- Manual testing with current release of Chrome, Safari and Firefox

### Attempts to Solve the issue

Created a repository at [pixelhandler/power-select-selected-broken].

- Clone the addon [cibernox/ember-power-select], and `npm link`…
  - Example local file path for cloned repo: `~/code/cibernox/ember-power-select`

    cd ~/code/cibernox/ember-power-select
    git co -b last-working-release v0.10.11
    npm link

- Clone the example code to demonstrate the issue…
  - Example local file path for cloned repo: `~/code/pixelhandler/power-select-selected-broken`

    cd ~/code/pixelhandler/power-select-selected-broken
    npm link ember-power-select
    ember test

*The test should pass**

- Change the version of the addon to a newer version

    cd ~/code/cibernox/ember-power-select
    git co -b current-beta-release v1.0.0-beta.8

- Update `package.json` in the example app

    cd ~/code/pixelhandler/power-select-selected-broken
    vim package.json

Change version to the current beta release, 1.0.0-beta.8

    -    "ember-power-select": "0.10.11",
    +    "ember-power-select": "1.0.0-beta.8",

- Run the test

    cd ~/code/pixelhandler/power-select-selected-broken
    ember test

*The test should fail**


[Working Twiddle]: https://ember-twiddle.com/4963fe6f1749f434ab962ca70c8bb4c8?openFiles=twiddle.json%2C
[pixelhandler/power-select-selected-broken]: https://gitlab.com/pixelhandler/power-select-selected-broken
[cibernox/ember-power-select]: https://github.com/cibernox/ember-power-select
