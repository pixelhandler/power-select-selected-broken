import { test } from 'qunit';
import moduleForAcceptance from 'power-select-selected-broken/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | index');

test('visiting /', function(assert) {
  visit('/');

  andThen(function() {
    let placeholder = find('.ember-power-select-placeholder').text().trim();
    assert.ok(placeholder);
  });
});
