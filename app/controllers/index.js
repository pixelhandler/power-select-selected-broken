import Ember from 'ember';

export default Ember.Controller.extend({

  matcher(item, query) {
    let rating = item.get('rating');
    return rating.toLowerCase().indexOf(query.toLowerCase());
  },

  actions: {
    selectedRating(model, rating) {
      model.set('rating', rating);
    }
  }
});
