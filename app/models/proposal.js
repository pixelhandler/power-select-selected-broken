import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Object.extend({
  name: '',
  desc: '',
  rating: null,
  ratings: Ember.computed(function() {
    let proxy = Ember.ArrayProxy.extend(Ember.PromiseProxyMixin, {
      'promise': new RSVP.Promise(function (resolve/*, reject*/) {
        Ember.run.later(function () {
          resolve(createList());
        }, 100);
      })
    });
    proxy = proxy.create();
    proxy.then(function(result) {
      proxy.set('content', result);
    });
    return proxy;
  }),
});

function createList() {
  let list = [];
  for (let i = 0; i < 10; i++) {
    list.push(Ember.Object.create({ rating: lang[i] }));
  }
  return list;
}

let lang = [
  'one',
  'two',
  'three',
  'four',
  'five',
  'six',
  'seven',
  'eight',
  'nine',
  'ten'
];
