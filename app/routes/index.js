import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    let factory = Ember.getOwner(this)._lookupFactory('model:proposal');
    return factory.create({
      name: 'Build something ambitions',
      desc: 'Start with Ember'
    });
  }
});
